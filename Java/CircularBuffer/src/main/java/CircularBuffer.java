public class CircularBuffer {

    private int size;
    private int capacity;
    private int firstValuePhysicalIndex;
    private int[] values;
    CircularBuffer(int newCapacity)
    {
        if (newCapacity < 1)
            throw new IllegalArgumentException();

        capacity = newCapacity;
        size = 0;
        values = new int[capacity];
        firstValuePhysicalIndex = 0;
    }
    int Size()
    {
        return size;
    }

    public void Push(int newValue) {
        if (size < capacity) {
            size++;
        }
        else
        {
            firstValuePhysicalIndex++;
        }

        values[ConvertToPhysicalIndex(size - 1)] = newValue;
    }

    public void Pop() {
        if (size == 0)
            throw new IndexOutOfBoundsException();

        size--;
    }

    public int Get(int index)
    {
        if (index >= size)
        {
            throw new IndexOutOfBoundsException();
        }
        return values[ConvertToPhysicalIndex(index)];
    }

    private int ConvertToPhysicalIndex(int virtualIndex) {
        return (firstValuePhysicalIndex + virtualIndex) % capacity;
    }
}
