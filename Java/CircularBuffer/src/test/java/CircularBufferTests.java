import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CircularBufferTests {

    private final int DefaultCapacity = 2;
    private CircularBuffer circularBuffer;
    private final int DefaultLastIndex = DefaultCapacity-1;

    private CircularBuffer makeCircularBuffer(int capacity) {
        return new CircularBuffer(capacity);
    }

    private void FillBuffer() {
        for (int index = 0; index < DefaultCapacity; index++)
            circularBuffer.Push(0);
    }

    @Before
    public void setUp() throws Exception {
        circularBuffer = new CircularBuffer(DefaultCapacity);
    }

    @Test(expected=IllegalArgumentException.class)
    public void ConstructWithCapacityLessThanOne_ExceptionIsThrown() throws Exception {
        makeCircularBuffer(0);
    }

    @Test
    public void AfterConstruction_SizeIsZero() throws Exception {
        Assert.assertEquals(0,circularBuffer.Size());
    }

    @Test
    public void PushAValue_SizeIsOne() throws Exception {
        circularBuffer.Push(1);

        Assert.assertEquals(1, circularBuffer.Size());
    }

    @Test
    public void BufferIsFull_PushAValue_SizeIsStillCapacity() throws Exception {
        FillBuffer();

        circularBuffer.Push(1);

        Assert.assertEquals(DefaultCapacity, circularBuffer.Size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void Empty_PopAValue_ExceptionThrown() throws Exception {

        circularBuffer.Pop();
    }

    @Test
    public void PushThenPop_SizeIsZero() throws Exception {
        circularBuffer.Push(5);
        circularBuffer.Pop();

        Assert.assertEquals(0, circularBuffer.Size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void Empty_GetFirstValue_ExceptionThrown() throws Exception {
        circularBuffer.Get(0);
    }

    @Test
    public void OneValueInBuffer_FirstValueIsPushedValue() throws Exception {
        circularBuffer.Push(5);

        Assert.assertEquals(5, circularBuffer.Get(0));
    }

    @Test
    public void PushTwoValues_FirstValueIsFirstPushedValue() throws Exception {
        circularBuffer.Push(5);
        circularBuffer.Push(2);

        Assert.assertEquals(5, circularBuffer.Get(0));
    }

    @Test
    public void PushTwoValues_SecondValueIsSecondPushedValue() throws Exception {
        circularBuffer.Push(5);
        circularBuffer.Push(2);

        Assert.assertEquals(2, circularBuffer.Get(1));
    }

    @Test
    public void BufferFull_PushNewValue_LastValueIsNewValue() throws Exception {

        FillBuffer();

        circularBuffer.Push(5);

        Assert.assertEquals(5, circularBuffer.Get(DefaultLastIndex));
    }

    @Test
    public void BufferFull_PushTwoNewValues_FirstValueIsFirstNewValue() throws Exception {
        FillBuffer();

        circularBuffer.Push(1);
        circularBuffer.Push(5);

        Assert.assertEquals(1, circularBuffer.Get(0));
    }

}
