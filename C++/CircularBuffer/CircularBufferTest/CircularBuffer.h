#include <memory>

class CircularBuffer
{
public:
	CircularBuffer(int capacity);
	int Size() const;
	void Push(int newValue);
	void Pop();
	int Get(int index) const;

private:
	int size;
	int capacity;
	int firstValuePhysicalIndex;
	std::unique_ptr<int []> values;

	int ConvertToPhysicalIndex(int virtualIndex) const;
};