#include "CppUnitTest.h"

#include "CircularBuffer.h"

#include <memory>
#include <functional>
#include <exception>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CircularBufferTest
{		
	TEST_CLASS(CircularBufferTest)
	{
	private:
		static const int DefaultCapacity = 2;
		static const int DefaultLastIndex = DefaultCapacity - 1;
		std::unique_ptr<CircularBuffer> circularBuffer;

		void FillBuffer()
		{
			for (int index = 0; index < DefaultCapacity; index++)
			{
			    circularBuffer->Push(0);
			}
		}

	public:
		
		TEST_METHOD_INITIALIZE(setup)
		{
			circularBuffer.reset(new CircularBuffer(DefaultCapacity));
		}

		TEST_METHOD(ConstructWithCapacityLessThanOne_ThrowsException)
		{
			try
			{
				circularBuffer.reset(new CircularBuffer(0));
				Assert::Fail(L"Expected invalid_argument exception");
			}
			catch(std::invalid_argument)
			{
			}
		}

		TEST_METHOD(NewlyConstructed_SizeIsZero)
		{
			Assert::AreEqual(0, circularBuffer->Size());
		}

		TEST_METHOD(PushAValue_SizeIsOne)
		{
			circularBuffer->Push(1);

			Assert::AreEqual(1, circularBuffer->Size());
		}

		TEST_METHOD(BufferFull_PushAValue_SizeIsStillCapacity)
		{
			FillBuffer();

			circularBuffer->Push(1);

			Assert::AreEqual(DefaultCapacity, circularBuffer->Size());
		}

		TEST_METHOD(BufferEmpty_PopAValue_ThrowsException)
		{
			try
			{
                circularBuffer->Pop();
				Assert::Fail(L"Expected out_of_range exception");
			}
			catch(std::out_of_range)
			{
			}

		}

		TEST_METHOD(BufferHasOneValue_Pop_SizeIsZero)
		{
			circularBuffer->Push(1);

			circularBuffer->Pop();

			Assert::AreEqual(0, circularBuffer->Size());
		}

		TEST_METHOD(BufferEmpty_GetFirstValue_ThrowsException)
		{
			try
			{
                circularBuffer->Get(0);
				Assert::Fail(L"Expected out_of_range exception");
			}
			catch(std::out_of_range)
			{
			}

		}

		TEST_METHOD(BufferHasOneValue_GetFirstValue_ReturnsPushedValue)
		{
             circularBuffer->Push(1); 

             Assert::AreEqual(1, circularBuffer->Get(0));
		}


		TEST_METHOD(BufferHasTwoValues_GetFirstValue_ReturnsFirstPushedValue)
		{
             circularBuffer->Push(1); 
			 circularBuffer->Push(2);

             Assert::AreEqual(1, circularBuffer->Get(0));
		}

		TEST_METHOD(BufferHasTwoValues_GetSecondValue_ReturnsSecondPushedValue)
		{
             circularBuffer->Push(1); 
			 circularBuffer->Push(2);

             Assert::AreEqual(2, circularBuffer->Get(1));
		}

		TEST_METHOD(BufferFull_PushNewValue_GetLastValueReturnsNewValue)
		{
			 FillBuffer();

			 circularBuffer->Push(2);

             Assert::AreEqual(2, circularBuffer->Get(DefaultLastIndex));
		}

		TEST_METHOD(BufferFull_PushTwoValues_GetFirstValueIsFirstNewValue)
		{
			 FillBuffer();

			 circularBuffer->Push(1);
			 circularBuffer->Push(2);

             Assert::AreEqual(1, circularBuffer->Get(0));
		}

	};
}