#include "CircularBuffer.h"

#include <stdexcept>

int CircularBuffer::ConvertToPhysicalIndex(int virtualIndex) const
{
	return (firstValuePhysicalIndex + virtualIndex) % capacity;
}

CircularBuffer::CircularBuffer(int newCapacity) : values(new int[newCapacity])
{
	if (newCapacity < 1)
	{
		throw std::invalid_argument("");
	}

	capacity = newCapacity;
	size = 0;
	firstValuePhysicalIndex = 0;
}

int CircularBuffer::Size() const
{
	return size;
}

void CircularBuffer::Push(int newValue)
{
	if (size < capacity)
	{
	    size++;
	}
	else
	{
		firstValuePhysicalIndex++;
	}

	values[ConvertToPhysicalIndex(size - 1)] = newValue;
}

void CircularBuffer::Pop()
{
	if (size == 0)
	{
	    throw std::out_of_range("");
	}
	size--;
}

int CircularBuffer::Get(int index) const
{
	if (index >= size)
	{
	    throw std::out_of_range("");
	}
	return values[ConvertToPhysicalIndex(index)];
}
