﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CircularBufferTest
{
    [TestClass]
    public class CircularBufferTest
    {
        private CircularBuffer circularBuffer;
        private const int DefaultCapacity = 2;
        private const int DefaultLastIndex = DefaultCapacity - 1;

        private void FillBuffer()
        {
            circularBuffer.Push(0);
            circularBuffer.Push(0);
        }

        [TestInitialize()]
        public void Initialize()
        {
            circularBuffer = new CircularBuffer(DefaultCapacity);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentOutOfRangeException))]
        public void CreateWithCapacityLessThanOne_ThrowException()
        {
            circularBuffer = new CircularBuffer(0);
        }

        [TestMethod]
        public void NewlyConstructed_SizeIsZero()
        {
            Assert.AreEqual(0, circularBuffer.Size());
        }

        [TestMethod]
        public void PushAValue_SizeIsOne()
        {
            circularBuffer.Push(1);

            Assert.AreEqual(1, circularBuffer.Size());
        }

        [TestMethod]
        public void BufferFull_PushAValue_SizeIsStillCapacity()
        {
            FillBuffer();

            circularBuffer.Push(1);

            Assert.AreEqual(DefaultCapacity, circularBuffer.Size());
        }

        [TestMethod]
        [ExpectedException(typeof(System.InvalidOperationException))]
        public void BufferEmpty_PopAValue_ThrowsException()
        {
            circularBuffer.Pop();
        }

        [TestMethod]
        public void BufferHasOneValue_PopAValue_SizeIsZero()
        {
            circularBuffer.Push(1);

            circularBuffer.Pop();

            Assert.AreEqual(0, circularBuffer.Size());
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentOutOfRangeException))]
        public void BufferEmpty_GetFirstValue_ThrowsException()
        {
            circularBuffer.Get(0);
        }

        [TestMethod]
        public void BufferHasOneValue_GetFirstValue_ReturnsTheValue()
        {
            circularBuffer.Push(1);

            Assert.AreEqual(1, circularBuffer.Get(0));
        }

        [TestMethod]
        public void BufferHasTwoValues_GetFirstValue_ReturnsTheFirstPushedValue()
        {
            circularBuffer.Push(2);
            circularBuffer.Push(3);

            Assert.AreEqual(2, circularBuffer.Get(0));
        }

        [TestMethod]
        public void BufferHasTwoValues_GetSecondValue_ReturnsTheSecondPushedValue()
        {
            circularBuffer.Push(2);
            circularBuffer.Push(3);

            Assert.AreEqual(3, circularBuffer.Get(1));
        }

        [TestMethod]
        public void BufferFull_PushNewValue_LastValueIsNewValue()
        {
            FillBuffer();

            circularBuffer.Push(3);

            Assert.AreEqual(3, circularBuffer.Get(DefaultLastIndex));
        }

        [TestMethod]
        public void BufferFull_PushEquivalentToCapacity_FirstValueIsFirstPushedValue()
        {
            FillBuffer();

            circularBuffer.Push(1);
            circularBuffer.Push(2);

            Assert.AreEqual(1, circularBuffer.Get(0));
        }
    }
}
