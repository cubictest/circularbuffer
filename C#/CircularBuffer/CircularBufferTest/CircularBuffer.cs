﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CircularBufferTest
{
    class CircularBuffer
    {
        private int capacity;
        private int size;
        private int[] values;
        private int firstValuePhysicalIndex;

        public CircularBuffer(int newCapacity)
        {
            if (newCapacity < 1)
            {
                throw new ArgumentOutOfRangeException();
            }
            this.size = 0;
            this.capacity = newCapacity;
            this.values = new int[newCapacity];
            this.firstValuePhysicalIndex = 0;
        }

        public int Size()
        {
            return size;
        }

        public void Push(int value)
        {
            if (size < capacity)
            {
                size++;
            }
            else
            {
                firstValuePhysicalIndex++;
            }

            values[ConvertToPhysicalIndex(size - 1)] = value;            
        }

        public void Pop()
        {
            if (size == 0)
            {
                throw new InvalidOperationException();
            }

            size--;
        }

        public int Get(int index)
        {
            if (index >= size)
            {
                throw new ArgumentOutOfRangeException();
            }

            return values[ConvertToPhysicalIndex(index)];
        }

        private int ConvertToPhysicalIndex(int virtualIndex)
        {
            return (firstValuePhysicalIndex + virtualIndex) % capacity;
        }
    }
}
